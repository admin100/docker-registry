# Docker Registry

### Description
Rapidly deploy a private Docker registry  (Designed for Debian 10). You can deploy multiple repositories on the
same host. These will all be proxied through nginx and secured with letsencrypt.

### Pre-requisites
1. Debian OS family jenkins host (Ubuntu/Debian). Preferably Debian 10
2. Non root user login with sudo privileges
3. Firewall setup to allow traffic through ports (http) 80 and (https)443 and of course ssh (22)
4. You will need Ansible installed on the machine used to deploy the jenkins server

### Setup
1. Edit `ansible/hosts.ini` to define the registry host (as mentioned in prerequisites)
2. Edit `ansible/hosts.ini` to set up the deployment variables; 'letsencrypt_email', 'domain_name' and 'port'
3. Run `ansible-playbook -i hosts.ini provision_registry.yml` to deploy to the server
4. The Docker registry credentials key should be printed to the console.

*NB: You can first run `ansible-playbook -i hosts.ini upgrade_stretch.yml` to upgrade a Debian stretch host to 
Debian buster if you need to.*

### Authentication

In order to give the project server access to pull images you will need to run:
`docker login -u <registry_username> -p <registry_password> {{ domain_name }}`

NOTE: If you don't want to expose the registry password in the bash history, rather:
`cat password.txt | docker-login -u <registry_user> --password-stdin {{ domain_name }}`

### Pushing and pulling images from the repo
Push:

1. `docker build . -t domain_name.com/project_name`
2. `docker push domain_name.com/project_name`

Pull:

1. `docker pull domain_name.com/project_name`

